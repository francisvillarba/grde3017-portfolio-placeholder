/**
 * index.js
 * 
 * Placeholder js for the placeholder personal portfolio website!
 * It is currently under construction and this entire site will eventually be replaced
 * by the real deal!
 * 
 * Please check back later :)
 * 
 * @author Francis Villarba
 */

function init() {
    console.log(`Francis Villarba's Personal Portfolio Website`);
    console.log(`Welcome`, `G'day and welcome to my portfolio website`);
    console.log(`Notice`, `As you can see, it is currently under construction!`);
    console.log(`Recommendation`, `Please check back later :)`);

    particlesJS.load('particles-js', 'particlesjs-config.json', function () {
        console.log(`Meanwhile`, `Have some particles!`);
    });
}

document.addEventListener('DOMContentLoaded', () => {
    init();
});